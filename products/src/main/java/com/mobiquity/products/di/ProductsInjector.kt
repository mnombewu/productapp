package com.mobiquity.products.di

import com.mobiquity.core.network.NetworkClientFactory
import com.mobiquity.products.data.repo.ProductRepositoryImpl
import com.mobiquity.products.data.service.ProductService
import com.mobiquity.products.presentation.ListProductsViewModel

object ProductsInjector {
    fun provideProductService(): ProductService = NetworkClientFactory.createService(ProductService::class.java)

    fun provideProductRepository() = ProductRepositoryImpl(provideProductService())

    fun provideProductViewModelFactory() = ListProductsViewModel.ListProductsViewModelFactory(provideProductRepository())
}