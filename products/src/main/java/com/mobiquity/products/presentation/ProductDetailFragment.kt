package com.mobiquity.products.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import coil.load
import com.mobiquity.core.Constants
import com.mobiquity.products.R
import com.mobiquity.products.databinding.FragmentProductDetailBinding

class ProductDetailFragment : Fragment() {
    private var _binding: FragmentProductDetailBinding? = null
    private val binding get() = _binding!!
    private val args: ProductDetailFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentProductDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding) {
            productNameTextView.text = args.product.name
            productPriceTextView.text = getString(R.string.pricing, args.product.salePrice.amount, args.product.salePrice.currency)
            //possibly hide or set fallback image
            productImageView.load("${Constants.BASE_URL}${args.product.url}") {
                crossfade(true)
                placeholder(R.drawable.ic_launcher_background)
            }
        }
    }
}