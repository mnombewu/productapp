package com.mobiquity.products.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import com.mobiquity.products.data.model.Product
import com.mobiquity.products.data.model.ProductCategory
import com.mobiquity.products.databinding.ListProductsFragmentBinding
import com.mobiquity.products.presentation.adapters.ProductAdapter

class ListProductsFragment : Fragment() {
    private var _binding: ListProductsFragmentBinding? = null
    private val binding get() = _binding!!

    companion object {
        private const val ARG_CATEGORY: String = "product_category"

        fun newInstance(productCategory: ProductCategory): ListProductsFragment {
            val args = Bundle()
            // this could all be swapped for VM implementation, but demo
            args.putParcelable(ARG_CATEGORY, productCategory)
            val fragment = ListProductsFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = ListProductsFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {
            if (it.containsKey(ARG_CATEGORY)) {
                (it.getParcelable(ARG_CATEGORY) as? ProductCategory)?.let { category ->
                    if (category.products.isEmpty()) {
                        binding.errorTextView.visibility = VISIBLE
                        binding.productRecyclerView.visibility = GONE
                    } else {
                        binding.productRecyclerView.adapter = ProductAdapter(category).apply {
                            onProductClickListener = object : ProductAdapter.OnProductClickListener {
                                override fun onClick(product: Product) {
                                    findNavController().navigate(ProductCategoryFragmentDirections.actionProductCategoryFragmentToProductDetailFragment(product))
                                }
                            }
                        }
                        binding.productRecyclerView.addItemDecoration(DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL))
                    }
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}