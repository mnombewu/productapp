package com.mobiquity.products.presentation.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.mobiquity.core.Constants.BASE_URL
import com.mobiquity.products.R
import com.mobiquity.products.data.model.Product
import com.mobiquity.products.data.model.ProductCategory
import com.mobiquity.products.databinding.ProductItemViewBinding

class ProductAdapter(private val productCategory: ProductCategory) : RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {
    var onProductClickListener: OnProductClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val binding: ProductItemViewBinding = ProductItemViewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ProductViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val product: Product = productCategory.products[position]
        holder.binding.productNameTextView.text = product.name
        holder.binding.root.setOnClickListener { onProductClickListener?.onClick(product) }
        //possibly hide or set fallback image
        holder.binding.productImageView.load("${BASE_URL}${product.url}") {
            crossfade(true)
            placeholder(R.drawable.ic_launcher_background)
        }
    }

    override fun getItemCount(): Int = productCategory.products.size

    inner class ProductViewHolder(val binding: ProductItemViewBinding) : RecyclerView.ViewHolder(binding.root)

    interface OnProductClickListener {
        fun onClick(product: Product)
    }
}