package com.mobiquity.products.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.tabs.TabLayoutMediator
import com.mobiquity.core.model.Resource
import com.mobiquity.products.databinding.FragmentProductCategoryBinding
import com.mobiquity.products.di.ProductsInjector
import com.mobiquity.products.presentation.adapters.ProductCategoryPagerAdapter

class ProductCategoryFragment : Fragment() {
    private var _binding: FragmentProductCategoryBinding? = null
    private val binding get() = _binding!!

    private val productsViewModel: ListProductsViewModel by lazy {
        val factory: ListProductsViewModel.ListProductsViewModelFactory = ProductsInjector.provideProductViewModelFactory()
        ViewModelProvider(this, factory).get(ListProductsViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentProductCategoryBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        productsViewModel.getProductCategories().observe(viewLifecycleOwner) {
            when (it.status) {
                Resource.Status.LOADING -> {
                    with(binding) {
                        circularProgressBar.visibility = View.VISIBLE
                        errorTextView.visibility = View.GONE
                        tabGroup.visibility = View.GONE
                    }
                }
                Resource.Status.ERROR -> {
                    with(binding) {
                        circularProgressBar.visibility = View.GONE
                        errorTextView.visibility = View.VISIBLE
                        tabGroup.visibility = View.GONE
                    }
                }
                Resource.Status.SUCCESS -> {
                    with(binding) {
                        circularProgressBar.visibility = View.GONE
                        errorTextView.visibility = View.GONE
                        tabGroup.visibility = View.VISIBLE
                        it.data?.let { categories ->
                            categoriesViewPager.adapter = ProductCategoryPagerAdapter(requireActivity(), categories)
                            TabLayoutMediator(tabLayout, categoriesViewPager) { tab, position ->
                                tab.text = categories[position].name
                            }.attach()
                        }
                    }
                }
            }
        }
    }
}