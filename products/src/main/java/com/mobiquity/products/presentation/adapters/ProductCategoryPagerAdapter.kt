package com.mobiquity.products.presentation.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.mobiquity.products.data.model.ProductCategories
import com.mobiquity.products.presentation.ListProductsFragment

class ProductCategoryPagerAdapter(fragmentActivity: FragmentActivity, private val categories: ProductCategories) : FragmentStateAdapter(fragmentActivity) {
    override fun getItemCount(): Int = categories.size

    override fun createFragment(position: Int): Fragment = ListProductsFragment.newInstance(categories[position])
}