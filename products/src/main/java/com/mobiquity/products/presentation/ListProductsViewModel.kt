package com.mobiquity.products.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.mobiquity.core.model.Resource
import com.mobiquity.products.data.model.ProductCategories
import com.mobiquity.products.data.repo.ProductRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

class ListProductsViewModel(private val productRepository: ProductRepository, private val dispatcher: CoroutineDispatcher = Dispatchers.IO) : ViewModel() {
    fun getProductCategories(): LiveData<Resource<ProductCategories>> = liveData(dispatcher) {
        emit(Resource.loading(null))
        try {
            //maybe cache this so that don't need to make calls regularly
            emit(Resource.success(productRepository.getProductCategories()))
        } catch (exception: Exception) {
            emit(Resource.error(exception))
        }
    }

    class ListProductsViewModelFactory(private val productRepository: ProductRepository) : ViewModelProvider.Factory {
        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ListProductsViewModel(productRepository) as T
        }
    }
}