package com.mobiquity.products.data.repo

import com.mobiquity.products.data.model.ProductCategories
import com.mobiquity.products.data.service.ProductService

class ProductRepositoryImpl(private val productService: ProductService) : ProductRepository {
    override suspend fun getProductCategories(): ProductCategories = productService.getProductCategories()
}