package com.mobiquity.products.data.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Product(
    val id: String,
    val name: String,
    val description: String,
    val categoryId: String,
    val salePrice: SalePrice,
    val url: String
) : Parcelable