package com.mobiquity.products.data.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ProductCategory(
    val id: String,
    val name: String,
    val description: String,
    val products: List<Product>
) : Parcelable