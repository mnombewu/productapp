package com.mobiquity.products.data.repo

import com.mobiquity.products.data.model.ProductCategories

interface ProductRepository {
    suspend fun getProductCategories(): ProductCategories
}