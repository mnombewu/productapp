package com.mobiquity.products.data.service

import com.mobiquity.products.data.model.ProductCategories
import retrofit2.http.GET

interface ProductService {
    @GET("/")
    @Throws(Exception::class)
    suspend fun getProductCategories(): ProductCategories
}