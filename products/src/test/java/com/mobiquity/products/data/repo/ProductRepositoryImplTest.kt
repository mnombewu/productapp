package com.mobiquity.products.data.repo

import com.mobiquity.products.data.model.Product
import com.mobiquity.products.data.model.ProductCategories
import com.mobiquity.products.data.model.ProductCategory
import com.mobiquity.products.data.model.SalePrice
import com.mobiquity.products.data.service.ProductService
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.doThrow
import org.mockito.Mockito.times
import org.mockito.junit.MockitoJUnitRunner
import java.io.IOException

@RunWith(MockitoJUnitRunner::class)
class ProductRepositoryImplTest {

    private lateinit var SUT: ProductRepository

    @Mock
    lateinit var productServiceMock: ProductService

    @Before
    fun setUp() {
        SUT = ProductRepositoryImpl(productServiceMock)
    }

    @Test
    fun getProductCategories_success_endpointCalled() {
        setupSuccessResponse()
        runBlocking {
            SUT.getProductCategories()
            Mockito.verify(productServiceMock, times(1)).getProductCategories()
            Mockito.verifyNoMoreInteractions(productServiceMock)
        }
    }

    @Test
    fun getProductCategories_success_categoriesReceived() {
        setupSuccessResponse()
        runBlocking {
            val result = SUT.getProductCategories()
            Assert.assertEquals(getProductCategories(), result)
        }
    }

    @Test
    fun getProduct_failure_networkFailure() {
        setupNetworkErrorResponse()
        Assert.assertThrows(IOException::class.java) {
            runBlocking { SUT.getProductCategories() }
        }
    }

    private fun setupSuccessResponse() {
        runBlocking {
            Mockito.`when`(productServiceMock.getProductCategories()).thenReturn(getProductCategories())
        }
    }

    private fun setupNetworkErrorResponse() {
        runBlocking {
            doThrow(IOException()).`when`(productServiceMock).getProductCategories()
        }
    }

    private fun getProductCategories(): ProductCategories {
        return ProductCategories().apply {
            add(ProductCategory("1", "Food", "", getProducts()))
        }
    }

    private fun getProducts(): List<Product> = listOf(Product("1", "Snack", "", "1", SalePrice("10", "ZAR"), ""))
}