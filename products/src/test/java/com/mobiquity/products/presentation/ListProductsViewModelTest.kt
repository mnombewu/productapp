package com.mobiquity.products.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.mobiquity.core.model.Resource
import com.mobiquity.products.data.model.Product
import com.mobiquity.products.data.model.ProductCategories
import com.mobiquity.products.data.model.ProductCategory
import com.mobiquity.products.data.model.SalePrice
import com.mobiquity.products.data.repo.ProductRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.*
import org.junit.runner.RunWith
import org.mockito.ArgumentCaptor
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.times
import org.mockito.junit.MockitoJUnitRunner
import javax.net.ssl.SSLHandshakeException

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class ListProductsViewModelTest {
    private lateinit var SUT: ListProductsViewModel

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var productRepositoryMock: ProductRepository

    private val testCoroutineDispatcher = TestCoroutineDispatcher()

    @Before
    fun setUp() {
        SUT = ListProductsViewModel(productRepositoryMock, testCoroutineDispatcher)
        Dispatchers.setMain(testCoroutineDispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testCoroutineDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun getProductCategories_success_serviceCalled() = runBlockingTest {
        Mockito.`when`(productRepositoryMock.getProductCategories()).thenReturn(getProductCategories())
        val observer = mock<Observer<Resource<ProductCategories>>>()

        SUT.getProductCategories().observeForever(observer)

        Mockito.verify(productRepositoryMock).getProductCategories()
        Mockito.verifyNoMoreInteractions(productRepositoryMock)
    }

    @Test
    fun getProductCategories_success_loadingAndSuccess() = runBlockingTest {
        Mockito.`when`(productRepositoryMock.getProductCategories()).thenReturn(getProductCategories())
        val observer = mock<Observer<Resource<ProductCategories>>>()
        val captor = argumentCaptor<Resource<ProductCategories>>()

        SUT.getProductCategories().observeForever(observer)

        Mockito.verify(observer, times(2)).onChanged(captor.capture())
        val values = captor.allValues
        Assert.assertEquals(loading().status, values[0].status)
        Assert.assertEquals(Resource.success(getProductCategories()).status, values[1].status)
    }

    @Test(expected = RuntimeException::class)
    fun getProductCategories_failure_networkError() = runBlockingTest {
        Mockito.`when`(productRepositoryMock.getProductCategories()).thenThrow(SSLHandshakeException("Test Exception"))
        val observer = mock<Observer<Resource<ProductCategories>>>()
        val captor = argumentCaptor<Resource<ProductCategories>>()

        SUT.getProductCategories().observeForever(observer)

        Mockito.verify(observer, times(2)).onChanged(captor.capture())
        val values = captor.allValues
        Assert.assertEquals(loading().status, values[0].status)
        Assert.assertEquals(error().status, values[1].status)
        Assert.assertEquals(1, uncaughtExceptions.size)
    }

    private fun loading(): Resource<ProductCategories> = Resource.loading()

    private fun error(): Resource<ProductCategories> = Resource.error()

    private fun getProductCategories(): ProductCategories {
        return ProductCategories().apply {
            add(ProductCategory("1", "Food", "", getProducts()))
        }
    }

    private fun getProducts(): List<Product> = listOf(Product("1", "Snack", "", "1", SalePrice("10", "ZAR"), ""))

    private inline fun <reified T> mock(): T = Mockito.mock(T::class.java)

    private inline fun <reified T> argumentCaptor(): ArgumentCaptor<T> = ArgumentCaptor.forClass(T::class.java)
}