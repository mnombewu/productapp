package com.mobiquity.core.model

class Resource<out T> private constructor(val status: Status, val data: T?, val throwable: Throwable?) {
    enum class Status {
        SUCCESS, ERROR, LOADING
    }

    companion object {
        fun <T> success(data: T): Resource<T> = Resource(Status.SUCCESS, data, null)

        fun <T> error(exception: Throwable? = null, data: T? = null): Resource<T> = Resource(Status.ERROR, data, exception)

        fun <T> loading(data: T? = null): Resource<T> = Resource(Status.LOADING, data, null)
    }
}