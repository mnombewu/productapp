package com.mobiquity.core.network

import com.mobiquity.core.Constants.BASE_URL
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object NetworkClientFactory {
    private const val CONNECTION_TIMEOUT = 10L
    private const val READ_TIMEOUT = 20L

    // android:usesCleartextTraffic="true" since https not allowed
    private fun getBaseUrl(): String = BASE_URL

    private fun getOkHttpClient(): OkHttpClient {
        //if release, log none
        val logLevel = HttpLoggingInterceptor.Level.BODY

        return OkHttpClient.Builder()
            .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
            .addInterceptor(HttpLoggingInterceptor().setLevel(logLevel))
            .build()
    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(getBaseUrl())
            .client(getOkHttpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    fun <T> createService(service: Class<T>): T = getRetrofit().create(service)
}
